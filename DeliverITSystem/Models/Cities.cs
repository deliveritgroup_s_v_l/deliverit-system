﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverITSystem.Models
{
   public class Cities
    {
        private string name;
        public Cities(string name)
        {
            this.Name = name;
        }
        public string Name { get; set; }
    }
}

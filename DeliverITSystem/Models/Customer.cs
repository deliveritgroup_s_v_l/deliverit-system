﻿using DeliverITSystem.Models.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverITSystem.Models
{
    public class Customer:Person,ICustomer
    {
        
        public Customer(string firstName, string lastName,string email) : base(firstName, lastName,email)
        {
            
        }
        
    }
}

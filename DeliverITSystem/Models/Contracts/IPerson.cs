﻿using System.Collections.Generic;

namespace DeliverITSystem.Models.Contracts
{
    public interface IPerson
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }


    }
}

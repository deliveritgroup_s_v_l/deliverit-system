﻿using System.Collections.Generic;
using DeliverITSystem.Models.Common;
using DeliverITSystem.Models.Contracts;
using System.Linq;

namespace DeliverITSystem.Models
{
    public abstract class Person:IPerson
    {
        private string firstName;
        private string lastName;
        private string email;

        public Person(string firstName, string lastName,string email)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
        }
        
        public string FirstName
        { 
            get => this.firstName;
            set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.PERSON_FIRSTNAME_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.PERSON_NAME_LEN_MIN, Constants.PERSON_NAME_LEN_MAX, Constants.PERSON_FIRSTNAME_LEN_ERR);
                this.firstName = value;
            } 
        }
        public string LastName
        {
            get => this.lastName;
            set
            {
                Validator.ValidateArgumentIsNotNull(value, Constants.PERSON_LASTNAME_NULL_ERR);
                Validator.ValidateIntRange(value.Length, Constants.PERSON_NAME_LEN_MIN, Constants.PERSON_NAME_LEN_MAX, Constants.PERSON_LASTNAME_LEN_ERR);
                this.lastName = value;
            }
        }
        public string Email 
        { 
            get => this.email;
            set
            {
                this.email = value;
            }
        }

    }
}

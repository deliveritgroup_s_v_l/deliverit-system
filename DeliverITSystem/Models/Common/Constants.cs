﻿
namespace DeliverITSystem.Models.Common
{
    class Constants
    {
        public const int PERSON_NAME_LEN_MIN = 2;
        public const int PERSON_NAME_LEN_MAX = 20;
        public const string PERSON_FIRSTNAME_NULL_ERR = "Person first name must not be NULL!";
        public static string PERSON_FIRSTNAME_LEN_ERR = $"Person first name must be between {PERSON_NAME_LEN_MIN} and {PERSON_NAME_LEN_MAX} characters long!";
        public const string PERSON_LASTNAME_NULL_ERR = "Person last name must not be NULL!";
        public static string PERSON_LASTNAME_LEN_ERR = $"Person last name must be between {PERSON_NAME_LEN_MIN} and {PERSON_NAME_LEN_MAX} characters long!";


    }
}
